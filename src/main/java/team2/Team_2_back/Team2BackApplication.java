package team2.Team_2_back;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;


@SpringBootApplication
//public class Team2BackApplication  implements CommandLineRunner {

//@CrossOrigin(origins = "*", allowedHeaders = "*" )
public class Team2BackApplication   {

	public static void main(String[] args) {
		SpringApplication.run(Team2BackApplication.class, args);
	}

/*
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	@Override
	public void run(String... args) throws Exception {
		String password = "abc12345";

		for(int i=0; i<4; i++){
			String passwordBcrypt = passwordEncoder.encode(password);
			System.out.println(passwordBcrypt);
		}
		System.out.println("Demo");
	}
 */
}
