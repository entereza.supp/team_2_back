package team2.Team_2_back.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team2.Team_2_back.entity.Productos;
import team2.Team_2_back.repository.ProductRepository;
import team2.Team_2_back.services.ProductService;
import team2.Team_2_back.services.UserDataService;
import team2.Team_2_back.util.ImageUtil;
import team2.Team_2_back.util.UserUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/api/product")
public class ProductController {
    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);


    @Autowired
    private ProductService productService;

    @Autowired
    public void setServices(ProductService productService){
        this.productService = productService;
    }


    @GetMapping()
    public void getProducts(){
        var user=UserUtil.getUser();
        System.out.println(user);
    }



    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public Productos saveCustomer(@RequestBody Productos productos){
        LOGGER.info("se registro un producto, {} ",productos);
        return productService.save(productos);
    }

    @RequestMapping(path = "/update", method = RequestMethod.PUT)
    public Productos updateCustomer(@RequestBody Productos product){
        LOGGER.info("SE ACTULIZO EL PRODUCTO");
        return productService.update(product);
    }

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public List<Productos> getAllCustomer(){

        LOGGER.info("SE ENCONTRO LOS PRODUCTOS");
        return productService.findAll();
    }


    @RequestMapping(path = "/delete", method = RequestMethod.DELETE)
    public void getDelete(@RequestParam(value = "idProducto") Integer idproducto){
        Productos productos= new Productos();
        productos.setIdproductos(idproducto);
        LOGGER.info("SE ELIMINO");
        productService.delete(productos);
    }


    @GetMapping(path="image/{path}/{name}" , produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getImage(@PathVariable String path, @PathVariable String name){
        ImageUtil storageUtil=new ImageUtil();
        byte[] image=storageUtil.getImage(path,name);
        return image;
    }

    @PutMapping(path="/{idproductos}/image", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity uploadImage(@RequestParam MultipartFile image, @PathVariable String idproductos){

        productService.uploadImage(image,Integer.parseInt(idproductos));
        return new ResponseEntity("Succesful process", HttpStatus.OK);
    }


}
