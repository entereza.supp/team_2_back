package team2.Team_2_back.api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import team2.Team_2_back.entity.DatosLogin;
import team2.Team_2_back.entity.DatosUsuario;
import team2.Team_2_back.entity.Usuario;
import team2.Team_2_back.services.UserDataService;

@RestController
@RequestMapping("/v1/api/user")
public class UserController {
    private static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserDataService userDataService;

    @Autowired
    public void setServices(UserDataService userDataService){
        this.userDataService = userDataService;
    }


    @RequestMapping(path = "/save", method = RequestMethod.POST)
    public DatosUsuario saveUserData(@RequestBody DatosUsuario user){
        LOGGER.info("Iniciando proceso para registrar usuario, {} ",user);
        //return null;
        return userDataService.saveUserData(user);
    }
    @RequestMapping(path = "/save/login", method = RequestMethod.POST)
    public DatosLogin saveLoginData(@RequestBody DatosLogin user){
        LOGGER.info("Iniciando proceso para registrar contraseña, {} ",user);
        return userDataService.saveLoginData(user);
    }

    @RequestMapping(path = "/save/full", method = RequestMethod.POST)
    public Usuario saveFullUser(@RequestBody Usuario usuario){
        LOGGER.info("Registro usuario, {} ",usuario);
        return userDataService.saveFullUser(usuario);
    }


}
