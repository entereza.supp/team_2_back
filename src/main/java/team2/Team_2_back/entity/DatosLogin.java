package team2.Team_2_back.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="datos_login")
public class DatosLogin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer iddatos_login;
    private String username;
    private String password;
    private Integer datos_usuario_iddatos_usuario;
    public DatosLogin(){

    }

    public Integer getIddatos_login() {
        return iddatos_login;
    }

    public void setIddatos_login(Integer iddatos_login) {
        this.iddatos_login = iddatos_login;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDatos_usuario_iddatos_usuario() {
        return datos_usuario_iddatos_usuario;
    }

    public void setDatos_usuario_iddatos_usuario(Integer datos_usuario_iddatos_usuario) {
        this.datos_usuario_iddatos_usuario = datos_usuario_iddatos_usuario;
    }
}
