package team2.Team_2_back.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity(name="datos_usuario")
public class DatosUsuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer iddatos_usuario;
    private String nombre;
    private String apellido;
    private String telefono;
    private String email;
    public DatosUsuario(){

    }

    public Integer getIddatos_usuario() {
        return iddatos_usuario;
    }

    public void setIddatos_usuario(Integer iddatos_usuario) {
        this.iddatos_usuario = iddatos_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
