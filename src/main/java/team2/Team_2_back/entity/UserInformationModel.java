package team2.Team_2_back.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserInformationModel extends User {
    private Integer idDatosLogin;
    private Integer idDatosUsuario;
    private String email;

    public UserInformationModel(String username,Integer idDatosLogin,Integer idDatosUsuario,String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.idDatosLogin=idDatosLogin;
        this.idDatosUsuario=idDatosUsuario;
        this.email=username;
    }


    public Integer getIdDatosLogin() {
        return idDatosLogin;
    }

    public void setIdDatosLogin(Integer idDatosLogin) {
        this.idDatosLogin = idDatosLogin;
    }

    public Integer getIdDatosUsuario() {
        return idDatosUsuario;
    }

    public void setIdDatosUsuario(Integer idDatosUsuario) {
        this.idDatosUsuario = idDatosUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public String toString() {
        return "UserInformationModel{" +
                "idDatosLogin=" + idDatosLogin +
                ", idDatosUsuario=" + idDatosUsuario +
                ", email='" + email + '\'' +
                '}';
    }
}

