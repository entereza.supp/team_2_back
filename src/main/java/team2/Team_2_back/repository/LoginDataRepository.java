package team2.Team_2_back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import team2.Team_2_back.entity.DatosLogin;


public interface LoginDataRepository extends JpaRepository<DatosLogin,String> {

    @Query(value = "select  * from datos_login  where username=  ? ", nativeQuery = true)
    DatosLogin getUserByUserName(String username);

    @Query(value = "SELECT LAST_INSERT_ID()", nativeQuery = true)
    Integer getLastInsertId();
}
