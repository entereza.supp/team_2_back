package team2.Team_2_back.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import team2.Team_2_back.entity.Productos;

import javax.transaction.Transactional;

@Service
public interface ProductRepository extends JpaRepository<Productos,String> {

    @Modifying
    @Transactional
    @Query(value="update productos p SET  p.direccion_imagen = ?1 WHERE p.idproductos = ?2", nativeQuery = true)
    public void updateimagen(String imagen, Integer idproducto);
}
