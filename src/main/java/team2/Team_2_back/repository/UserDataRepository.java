package team2.Team_2_back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import team2.Team_2_back.entity.DatosUsuario;

public interface UserDataRepository extends JpaRepository<DatosUsuario,String> {


}
