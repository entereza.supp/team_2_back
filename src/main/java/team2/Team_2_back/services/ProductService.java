package team2.Team_2_back.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import team2.Team_2_back.entity.Productos;
import team2.Team_2_back.repository.LoginDataRepository;
import team2.Team_2_back.repository.ProductRepository;
import team2.Team_2_back.repository.UserDataRepository;
import team2.Team_2_back.util.ImageUtil;

import java.util.List;

@Service
public class ProductService {
    private ProductRepository productRepository;

    @Autowired
    public void setRepo(ProductRepository productRepository){
        this.productRepository = productRepository;
    }


    public Productos save(Productos productos) {
        return productRepository.save(productos);
    }


    public Productos update(Productos product) {
        return productRepository.save(product);
    }

    public List<Productos> findAll() {
        return productRepository.findAll();
    }


    public void delete(Productos productos) {
        productRepository.delete(productos);
    }


    public void uploadImage(MultipartFile image, Integer idproducto) {
        ImageUtil imageUtil = new ImageUtil();
        String newImageName = imageUtil.uploadImage(image,"images","product",idproducto);
        productRepository.updateimagen(newImageName,idproducto);
    }
}
