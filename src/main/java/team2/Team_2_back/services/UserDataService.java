package team2.Team_2_back.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import team2.Team_2_back.entity.DatosLogin;
import team2.Team_2_back.entity.DatosUsuario;
import team2.Team_2_back.entity.Usuario;
import team2.Team_2_back.repository.LoginDataRepository;
import team2.Team_2_back.repository.UserDataRepository;

@Service
public class UserDataService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    private LoginDataRepository loginDataRepository;
    private UserDataRepository userDataRepository;
    @Autowired
    public void setRepo(LoginDataRepository loginDataRepository,UserDataRepository userDataRepository){
        this.loginDataRepository = loginDataRepository;
        this.userDataRepository = userDataRepository;
    }


    public DatosLogin saveLoginData(DatosLogin user) {

        String passwordBcrypt = passwordEncoder.encode(user.getPassword());
        user.setPassword(passwordBcrypt);
        return loginDataRepository.save(user);
    }

    public DatosUsuario saveUserData(DatosUsuario user) {

        return userDataRepository.save(user);
    }

    public Usuario saveFullUser(Usuario usuario) {
        DatosUsuario datosUsuario = new DatosUsuario();
        datosUsuario.setNombre(usuario.getNombre());
        datosUsuario.setApellido(usuario.getApellido());
        datosUsuario.setEmail(usuario.getEmail());
        datosUsuario.setTelefono(usuario.getTelefono());
        userDataRepository.save(datosUsuario);
        DatosLogin datosLogin = new DatosLogin();
        datosLogin.setUsername(usuario.getUsername());
        String passwordBcrypt = passwordEncoder.encode(usuario.getPassword());
        datosLogin.setPassword(passwordBcrypt);
        datosLogin.setDatos_usuario_iddatos_usuario(loginDataRepository.getLastInsertId());
        loginDataRepository.save(datosLogin);
        return usuario;
    }
}
