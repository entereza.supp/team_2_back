package team2.Team_2_back.services;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import team2.Team_2_back.entity.DatosLogin;
import team2.Team_2_back.entity.UserInformationModel;
import team2.Team_2_back.repository.LoginDataRepository;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Resource
    private LoginDataRepository loginDataRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        DatosLogin user = loginDataRepository.getUserByUserName(username);
        System.out.println("user"+user.toString());
        if(user!=null){
            UserInformationModel userInformationModel=new UserInformationModel(user.getUsername(), user.getIddatos_login(),user.getDatos_usuario_iddatos_usuario(),user.getPassword(), List.of(new SimpleGrantedAuthority("ROLE_USER")));
            return userInformationModel;

        }
        else{
            throw new UsernameNotFoundException("User '"+username+"' not found!");
        }
    }


}
