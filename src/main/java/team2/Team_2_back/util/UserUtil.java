package team2.Team_2_back.util;

import org.springframework.security.core.context.SecurityContextHolder;
import team2.Team_2_back.entity.UserInformationModel;

public class UserUtil {
    static public UserInformationModel getUser(){
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        UserInformationModel user= (UserInformationModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user;
    }
}