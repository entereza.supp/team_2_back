package team2.Team_2_back.util.security;

//import bo.ucb.edu.vic19.dao.AuthDao;
//import bo.ucb.edu.vic19.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;
import team2.Team_2_back.entity.DatosLogin;
import team2.Team_2_back.repository.LoginDataRepository;

import java.util.HashMap;
import java.util.Map;

@Component
public class InfoAdditionalToken implements TokenEnhancer{

    @Autowired
    private LoginDataRepository loginDataRepository;

    // Se genera los datos del AuthDao.
    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken oAuth2AccessToken, OAuth2Authentication oAuth2Authentication) {
        Map<String, Object> info = new HashMap<>();
        DatosLogin user =loginDataRepository.getUserByUserName(oAuth2Authentication.getName());
        if(user != null){
            info.put("usernameId", user.getIddatos_login());
            info.put("username", user.getUsername());
            //info.put("userName", user.getUserName());
            //info.put("role", 1);
        }
        ((DefaultOAuth2AccessToken) oAuth2AccessToken).setAdditionalInformation(info);

        return oAuth2AccessToken;
    }
}
